module gitee.com/gopher2011/gin

go 1.13

require (
	gitee.com/gopher2011/function v0.1.1
	github.com/appleboy/gofight/v2 v2.1.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.5.0
	github.com/goccy/go-json v0.4.13
	github.com/gogf/gf v1.15.6
	github.com/golang/protobuf v1.3.3
	github.com/json-iterator/go v1.1.10
	github.com/mattn/go-isatty v0.0.12
	github.com/stretchr/testify v1.7.0
	github.com/tidwall/gjson v1.7.5
	github.com/ugorji/go/codec v1.2.5
	gopkg.in/yaml.v2 v2.4.0
)
