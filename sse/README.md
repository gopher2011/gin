## Sample code

```go
import "github.com/gin-contrib/sse"

func httpHandler(w http.ResponseWriter, req *http.Request) {
	// data can be a primitive like a string, an integer or a float
	sse.Encode(w, sse.Event{
		Event: "message",
		Data:  "some data\nmore data",
	})

	// also a complex type, like a map, a struct or a slice
	sse.Encode(w, sse.Event{
		Id:    "124",
		Event: "message",
		Data: map[string]interface{}{
			"user":    "manu",
			"date":    time.Now().Unix(),
			"content": "hi!",
		},
	})
}
```
```
event: message
data: some data\\nmore data

id: 124
event: message
data: {"content":"hi!","date":1431540810,"user":"manu"}
 
```

## Content-Type

```go
fmt.Println(sse.ContentType)
```
```
text/event-stream
```

## Decoding support

There is a client-side implementation of SSE coming soon.
