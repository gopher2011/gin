package bind

import (
	"github.com/gogf/gf/util/gvalid"
	"sync"
)
// 通过nil断言 *gValidValidator 实现了 IStructValidator。
var _ IValidator = (*gValidValidator)(nil)

type gValidValidator struct {
	validate *gvalid.Validator
	once sync.Once
}

func (v *gValidValidator) lazyInit() {
	v.once.Do(func() {
		v.validate = gvalid.New()
	})
}

// Engine 根据具体实现返回一个验证器。
//  通过断言，获取具体的验证器。
//  va,ok := IValidator.Engine().(*gvalid.Validator)
func (v *gValidValidator) Engine() interface{} {
	v.lazyInit()
	return v.validate
}

// Validate 接收任何类型的参数，但仅验证 struct/*struct。支持结构体嵌套校验。
//  详情请看 https://goframe.org/pages/viewpage.action?pageId=1114282
func (v *gValidValidator) Validate(obj interface{}) error {
	var err *gvalid.Error
	v.lazyInit()
	err= v.validate.CheckStruct(obj,nil)
	return err
}






