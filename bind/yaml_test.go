package bind

import (
	"fmt"
	"testing"
)

func TestYAMLBindingBindBody(t *testing.T) {
	var s struct {
		Foo string `yaml:"foo"`
	}
	err := yamlBinding{}.BindBody([]byte("foo: FOO"), &s)
	fmt.Println(err)
	//require.NoError(t, err)
	//assert.Equal(t, "FOO", s.Foo)
}
