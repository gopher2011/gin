package bind

//import (
//	"fmt"
//	"github.com/gogf/gf/util/gvalid"
//	"testing"
//)
/*
// SignUpParam 用户注册参数
type SignUpParam struct {
	Username   string `json:"username" form:"username" v:"required|length:6,30#请输入用户名|用户名长度应当在:min到:max之间"`  // 用户名
	Password   string `json:"password" form:"password" v:"password@required|length:6,16#请输入密码|密码长度应当在:min到:max之间"`    // 密码
	RePassword string `json:"rePassword" form:"rePassword" v:"required|same:password#请输入密码|两次密码不一致，请重新输入"` // 重复密码
	Nickname   string `json:"nickname" form:"nickname" v:"required#请输入中文名"`                                  // 中文名
	Email      string `json:"email" form:"email" v:"required|email#请输入邮箱|邮箱不合法"`                                // 邮箱
}

type Pass struct {
	Pass1 string `v:"password1@required|same:password2#请输入您的密码|您两次输入的密码不一致"`
	Pass2 string `v:"password2@required|same:password1#请再次输入您的密码|您两次输入的密码不一致"`
}

type User struct {
	Pass
	Id   int
	Name string `v:"name@required#请输入您的姓名"`
}

func TestGValidValidator(t *testing.T) {
	sp := &SignUpParam{
		Username: "1234567",
		Password: "123456",
		RePassword: "123456",
		Nickname: "admin",
		Email: "166@qq.com",
	}

	user := &User{
		Name: "john",
		Pass: Pass{
			Pass1: "1",
			Pass2: "2",
		},
	}

	vs := new(gValidValidator)
	err := vs.Validate(sp)
	err2 := vs.Validate(user)
	fmt.Println("err1=",err)
	fmt.Println("err2=",err2)

	if va,ok := vs.Engine().(*gvalid.Validator); ok {
		err3 := va.CheckStruct(user, nil)
		fmt.Println("err3=",err3)
	}

	err5 := validate(user)
	fmt.Println("err5=",err5)
}*/