package bind

import (
	"fmt"
	"testing"
)

func TestJSONBindingBindBody(t *testing.T) {
	var s struct {
		Foo string `json:"foo"`
	}
	err := jsonBinding{}.BindBody([]byte(`{"foo": "FOO"}`), &s)
	fmt.Println(err)
	//require.NoError(t, err)
	//assert.Equal(t, "FOO", s.Foo)
}

func TestJSONBindingBindBodyMap(t *testing.T) {
	s := make(map[string]string)
	err := jsonBinding{}.BindBody([]byte(`{"foo": "FOO","hello":"world"}`), &s)
	fmt.Println(err)
	//require.NoError(t, err)
	//assert.Len(t, s, 2)
	//assert.Equal(t, "FOO", s["foo"])
	//assert.Equal(t, "world", s["hello"])
}
