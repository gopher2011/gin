//go:build !nomsgpack
// +build !nomsgpack

package bind

import (
	"bytes"
	"fmt"

	//"github.com/bytedance/go-tagexpr/v2"
	"github.com/stretchr/testify/assert"
	"github.com/ugorji/go/codec"
	"testing"
)

func TestBindingMsgPack(t *testing.T) {
	test := FooStruct{
		Foo: "bar",
	}

	h := new(codec.MsgpackHandle)
	assert.NotNil(t, h)
	buf := bytes.NewBuffer([]byte{})
	assert.NotNil(t, buf)
	err := codec.NewEncoder(buf, h).Encode(test)
	assert.NoError(t, err)

	data := buf.Bytes()

	testMsgPackBodyBinding(t,
		MsgPack, "msgpack",
		"/", "/",
		string(data), string(data[1:]))
}

func testMsgPackBodyBinding(t *testing.T, b IBind, name, path, badPath, body, badBody string) {
	assert.Equal(t, name, b.Name())

	obj := FooStruct{}
	req := requestWithBody("POST", path, body)
	req.Header.Add("Content-Type", MiMeMSGPACK)
	err := b.Bind(req, &obj)
	//assert.NoError(t, err)
	//assert.Equal(t, "bar", obj.Foo)
	fmt.Println(err)

	obj = FooStruct{}
	req = requestWithBody("POST", badPath, badBody)
	req.Header.Add("Content-Type", MiMeMSGPACK)
	err = MsgPack.Bind(req, &obj)
	//assert.Error(t, err)
	fmt.Println(err)
}

func TestBindingDefaultMsgPack(t *testing.T) {
	assert.Equal(t, MsgPack, Default("POST", MiMeMSGPACK))
	assert.Equal(t, MsgPack, Default("PUT", MiMeMSGPACK2))
}
