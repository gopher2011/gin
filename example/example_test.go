package example

import (
	"gitee.com/gopher2011/function/gdoc/swagger"
	"gitee.com/gopher2011/gin"
	"github.com/gogf/gf/util/gvalid"
	"testing"
)


// 绑定uri
type Person struct {
	ID string `uri:"id" v:"required"`
	Name string `uri:"name" v:"required"`
}

// 原文链接 https://studygolang.com/topics/11493
type MsgJson struct {
	Msg string `json:"msg"`
}

func BindAndValid(c *gin.Context, params interface{}) *gvalid.Error {
	_ = c.Parse(params) // 展示校验库，就先不多写err判断了
	// 校验
	if err := gvalid.CheckStruct(params, nil); err != nil {
		return err
	}
	return nil
}

// SignUpParam 用户注册参数
type SignUpParam struct {
	Username   string `json:"username" form:"username" v:"required|length:6,30#请输入用户名|用户名长度应当在:min到:max之间"`  // 用户名
	Password   string `json:"password" form:"password" v:"password@required|length:6,16#请输入密码|密码长度应当在:min到:max之间"`    // 密码
	RePassword string `json:"rePassword" form:"rePassword" v:"required|same:password#请输入密码|两次密码不一致，请重新输入"` // 重复密码
	Nickname   string `json:"nickname" form:"nickname" v:"required#请输入中文名"`                                  // 中文名
	Email      string `json:"email" form:"email" v:"required|email#请输入邮箱|邮箱不合法"`                                // 邮箱
}

func Init(){
	r := gin.New()

	// 将form表单请求参数解析到 SignUpParam中。
	r.POST("/login", func(c *gin.Context) {
		var l SignUpParam
		err := c.Parse(&l)
		if err != nil {
			c.JSON(400,err.Error())
		}else {
			num := len(c.Params)
			c.JSON(200,gin.H{"msg":"success","urlParams":num,"data":l})
		}
	})

	// 将 uri请求参数解析到结构体中
	r.GET("/:name/:id", func(c *gin.Context) {
		var person Person
		if err := c.Parse(&person); err != nil {
			c.JSON(400,err.Error())
		}else {
			num := len(c.Params)
			c.JSON(200,gin.H{"name":person.Name,"uuid":person.ID,"data":num})
		}
	})

	r.POST("/signup", func(c *gin.Context) {
		var p SignUpParam
		if err := BindAndValid(c,&p); err != nil {
			c.JSON(400,err.Error())
		}
	})

	r.POST("/test", func(c *gin.Context) {
		var a,b SignUpParam
		err := c.ParseBody(&a)
		if err != nil {
			c.JSON(400,err.Error())
		}else {
			c.JSON(200,gin.H{"msg":"success","data":a})
		}
		err2 := c.ParseBody(&b)
		if err != nil {
			c.JSON(400,err2.Error())
		}else {
			c.JSON(200,gin.H{"msg":"success","data":b})
		}
	})

	r.Run(":8080")
}

func Test(t *testing.T) {
	Init()
}

//func Hello(r *gin.Context){
//	id := r.Param("id")
//	r.JSON(200,id)
//}

type Param struct {
	A string `json:"a" form:"a"`
	B string `json:"b" form:"b"`
	C int	`json:"c" form:"c"`
}

func Println(r *gin.Context){
	var a Param
	//r.ShouldBind(&a)
	r.Parse(&a)
	r.JSON(200,a)
}

func Update(r *gin.Context){
	str := r.Param("id")
	var a Param
	//r.ShouldBind(&a)
	r.Parse(&a)
	r.JSON(200,gin.H{"data":a,"id":str})
}

//func Base(g *gin.RouterGroup){
//	r := g.Group("/base")
//	r.GET("/:id",Hello)
//	r.GET("/hello",Println)
//	r.POST("/update/:id",Update)
//}

type ReqTest struct {
	AccessToken string `json:"access_token"`
	UserName    string `json:"user_name"` // 带校验方式
	Password    string `json:"password"`
}
type Hell struct {}

// @Router /block [post,get]
func (s *Hell) Hello(c *gin.Context){
	var ptr ReqTest
	c.Parse(&ptr)
	c.JSON(200,ptr) // 返回结果
}

type Sys struct {

}

// Hello 带注解路由(参考beego形式)
// @Router /print [post,get]
func (s *Sys) Hell(c *gin.Context){
	var ptr ReqTest
	c.Parse(&ptr)
	c.JSON(200,ptr) // 返回结果
}

//type ReqTest struct {
//	AccessToken string `json:"access_token"`
//	UserName    string `json:"user_name"` // 带校验方式
//	Password    string `json:"password"`
//}
//
//type Hello struct {}

// Hello 带注解路由(参考beego形式)
// @Router /block [post,get]
//func (s *Hello) Hello(c *gin.Context, req *ReqTest) {
//	fmt.Println(req)
//	c.JSON(200,req) // 返回结果
//}

//func Print(c *gin.Context){
//	c.JSON(200,"hello word")
//	c.Next()
//}

//// @Router /hello [post]
//func (s *Hello) Hello1(c *gin.Context) {
//	var ptr ReqTest
//	c.Parse(&ptr)
//	c.JSON(200,ptr) // 返回结果
//}
//
//// Hello2 不带注解路由(参数为2默认post)
//func (s *Hello) Hello2(c *gin.Context, req ReqTest) {
//	fmt.Println(req)
//	c.JSON(http.StatusOK, "ok") // gin 默认返回结果
//}

//// Hello3 [grpc-go](https://github.com/grpc/grpc-go) 模式
//func (s *Hello) Hello3(c *gin.Context, req ReqTest) (*ReqTest, error) {
//	fmt.Println(req)
//	return &req,nil
//}

//type Sys struct {}
//
//// @Router /print [post,get]
//func (s *Sys) Hell(c *gin.Context){
//	var ptr ReqTest
//	c.Parse(&ptr)
//	c.JSON(200,ptr) // 返回结果
//}


var list []interface{}

func TestInit(t *testing.T) {
	swagger.SetHost("https://localhost:8080")
	swagger.SetBasePath("example")
	swagger.SetSchemes(true, false)

	router := gin.Default()
	group := router.Group("/base")
	list = []interface{}{
		//&Hello{},&Sys{},
		&Hell{},&Sys{},
	}


	router.Register(group,list)
	//Base(group)
	router.Run(":8080")
}

